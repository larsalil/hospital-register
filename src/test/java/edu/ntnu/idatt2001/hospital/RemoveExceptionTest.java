package edu.ntnu.idatt2001.hospital;

import edu.ntnu.idatt2001.hospital.exception.RemoveException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@Nested
@DisplayName("Person is removed from list")
class PersonIsRemovedFromListTest {
    Department department;
    Employee employee;
    Patient patient;

    @BeforeEach
    @DisplayName("Fill department with test data")
    void fillDepartmentWithData() {
        department = new Department("R&D");
        employee = new Employee("Lars Andreas", "Lillehaug", "1");
        patient = new Patient("Stud", "Ass", "2");
        department.addEmployee(employee);
        department.addPatient(patient);
    }

    @Test
    @DisplayName("Employee is removed from list")
    void employeeIsRemovedFromEmployeeList() throws RemoveException {
        department.remove(employee);
        assertTrue(department.employees.isEmpty());
    }

    @Test
    @DisplayName("Patient is removed from list")
    void patientIsRemovedFromPatientList() throws RemoveException {
        department.remove(patient);
        assertTrue(department.patients.isEmpty());
    }
}

@Nested
@DisplayName("Person is not in list")
class PersonIsNotInListTest {
    Department department;
    Employee employee;
    Patient patient;

    @BeforeEach
    @DisplayName("Make an empty department")
    void makeEmptyDepartment() {
        department = new Department("R&D");
        //People to try and remove
        employee = new Employee("Tull", "Tøys", "1");
        patient = new Patient("Ædda", "Bædda", "2");

    }

    @Test
    @DisplayName("Employee is not in list")
    void employeeIsNotInEmployeeList() {
        assertThrows(RemoveException.class, () -> department.remove(employee));
    }

    @Test
    @DisplayName("Patient is not in list")
    void patientIsNotInPatientList() {
        assertThrows(RemoveException.class, () -> department.remove(patient));
    }
}