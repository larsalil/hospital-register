package edu.ntnu.idatt2001.hospital.healthpersonal.doctor;

import edu.ntnu.idatt2001.hospital.Employee;
import edu.ntnu.idatt2001.hospital.Patient;

/**
 * The type Doctor.
 */
public abstract class Doctor extends Employee {

    /**
     * Instantiates a new Person.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets diagnosis of a given patient.
     *
     * @param patient   the patient
     * @param diagnosis the diagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
