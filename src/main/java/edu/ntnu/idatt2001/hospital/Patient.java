package edu.ntnu.idatt2001.hospital;

/**
 * The type Patient.
 */
public class Patient extends Person implements Diagnosable{
    private String diagnosis = "";

//Constructor

    /**
     * Instantiates a new Person.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

//Methods

    /**
     * Gets diagnosis.
     *
     * @return the diagnosis
     */
    protected String getDiagnosis() {
        return diagnosis;
    }

    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Diagnosis: " + diagnosis + "\n";
    }
}
