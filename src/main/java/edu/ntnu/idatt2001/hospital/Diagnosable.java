package edu.ntnu.idatt2001.hospital;

/**
 * The interface Diagnosable.
 */
public interface Diagnosable {

    /**
     * Sets diagnosis.
     *
     * @param diagnosis the diagnosis
     */
    void setDiagnosis(String diagnosis);
}
