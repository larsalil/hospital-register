package edu.ntnu.idatt2001.hospital;

import java.util.ArrayList;

/**
 * The type Hospital.
 */
public class Hospital {
    private final String hospitalName;
    /**
     * The Departments.
     */
    ArrayList<Department> departments = new ArrayList<>();

//Constructor

    /**
     * Instantiates a new Hospital.
     *
     * @param hospitalName the hospital name
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
    }

//Methods

    /**
     * Gets hospital name.
     *
     * @return the hospital name
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Gets departments.
     *
     * @return the departments
     */
    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * Add department.
     *
     * @param department the department
     */
    public void addDepartment(Department department) {
        departments.add(department);
    }

    @Override
    public String toString() {
        String out = "\n";
        out += ("Hospital name: " + hospitalName);
        for (Department department : departments) {
            out += department;
        }
        out += "\n\n";
        return out;
    }
}
