package edu.ntnu.idatt2001.hospital;

import edu.ntnu.idatt2001.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

/**
 * The type Department.
 */
public class Department {
    private String departmentName;
    /**
     * The Employees.
     */
    ArrayList<Employee> employees = new ArrayList<>();
    /**
     * The Patients.
     */
    ArrayList<Patient> patients = new ArrayList<>();

//Constructor
    /**
     * Instantiates a new Department.
     *
     * @param departmentName the department name
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
    }

//Methods

    /**
     * Sets department name.
     *
     * @param departmentName the department name
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Gets department name.
     *
     * @return the department name
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Gets employees.
     *
     * @return the employees
     */
    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    /**
     * Add employee.
     *
     * @param employee the emp
     */
    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    /**
     * Gets patients.
     *
     * @return the patients
     */
    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * Add patient.
     *
     * @param patient the pat
     */
    public void addPatient(Patient patient) {
        patients.add(patient);
    }

    /**
     * Remove a person from patients or employees.
     * Throws RemoveException if the person isn't in any of the lists.
     * Assumes a person cant be both an employee and patient.
     *
     * @param person the person
     */
    @SuppressWarnings("SuspiciousMethodCalls")
    public void remove(Person person) throws RemoveException {
        if (employees.contains(person)) {
            employees.remove(person);
        } else if (patients.contains(person)) {
            patients.remove(person);
        } else {
            throw new RemoveException(person.getFullName());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName) && Objects.equals(employees, that.employees) && Objects.equals(patients, that.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    @Override
    public String toString() {
        String out = "\n";
        out += ("Department: " + departmentName + "\n");
        for (Employee employee : employees) {
            out += employee;
        }
        for (Patient patient : patients) {
            out += patient;
        }
        return out;
    }
}
