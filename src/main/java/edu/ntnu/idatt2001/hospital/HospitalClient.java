package edu.ntnu.idatt2001.hospital;

import edu.ntnu.idatt2001.hospital.exception.RemoveException;

public class HospitalClient {
    public static void main(String[] args) {
        //Fill a hospital with test data
        Hospital hospital = new Hospital("UnSick");
        HospitalTestData.fillRegisterWithTestData(hospital);

        //Calling remove() to remove an employee
        Person employee = new Employee("Inco", "Gnito", "");
        try {
            hospital.getDepartments().get(0).remove(employee);
            System.out.println(employee.getFullName() + " successfully removed.");
        } catch (RemoveException e) {
            e.printStackTrace();
        }

        //Calling remove() to attempt to remove a patient which doesn't exist in department 2
        Person patient = new Patient("Hypo", "Konder", "1");
        try {
            hospital.getDepartments().get(1).remove(patient);
        } catch (RemoveException e) {
            System.out.println(e.getMessage());
        }
    }
}
