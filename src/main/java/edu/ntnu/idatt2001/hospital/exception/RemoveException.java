package edu.ntnu.idatt2001.hospital.exception;

public class RemoveException extends Exception {
    private static final long serialVersionUID = 1L;

    public RemoveException(String s) {
        super(s + " could not be found in the register.");
    }
}
